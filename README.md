<!--
SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen

SPDX-License-Identifier: CC0-1.0
-->

⚠ THIS REPOSITORY HAS BEEN ARCHIVED IN FAVOR OF https://gitlab.gwdg.de/fe/roger/core ⚠

# ROGER Frontend Component

## Development

Install dependencies.

```sh
npm run i-all
```

Start [dev server](http://localhost:3000).

```sh
npm run dev
```

Run [tests](__tests__).

```sh
npm run test
```

Updated [context.json](src/utils/context.json)? Regenerate `IRIs.js`.

```sh
npm run generate-prefixes
```

Test the production build of the component library (with profiling enabled).

```sh
npm run build-prod
npm run prod
```

## Framework Documentation Entrypoints

- [REACT](https://reactjs.org/docs/getting-started.html)
- [MUI](https://mui.com/getting-started/installation/)
- [JEST](https://jestjs.io/docs/tutorial-react)

## Contributing

This repo is [![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/) and uses [husky🐶](https://www.npmjs.com/package/husky) to ensure compliance with [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).

## License

This project aims to be [REUSE compliant](https://api.reuse.software/info/gitlab.gwdg.de/fe/roger-frontend).
Original parts are licensed under EUPL-1.2.
Derivative code is licensed under the respective license of the original.
Documentation, configuration and generated code files are licensed under CC0-1.0.

## Badges

[![REUSE status](https://api.reuse.software/badge/gitlab.gwdg.de/fe/roger-frontend)](https://api.reuse.software/info/gitlab.gwdg.de/fe/roger-frontend)
