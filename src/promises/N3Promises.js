// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

/**
 * Wrap the N3Parser parse call, so that it returns a Promise.
 * @param {N3.Parser} parser
 * @param {String} input
 * @param {N3.Store} store
 * @returns Promise
 */
const parsePromise = (parser, input, store) => {
  return new Promise((resolve, reject) => {
    parser.parse(input, (error, quad, prefixes) => {
      if (error) return reject(error);
      if (quad) {
        store.add(quad);
      }
      resolve(store, prefixes);
    });
  });
};

/**
 * Wrap the N3Writer end call, so that it returns a Promise.
 * @param {N3.Writer} writer
 * @returns
 */
const endPromise = (writer) => {
  return new Promise((resolve, reject) => {
    writer.end((error, result) => {
      if (error) return reject(error);
      if (result) return resolve(result);
    });
  });
};

export { endPromise, parsePromise };
