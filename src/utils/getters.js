// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { KEY_ID, KEY_LANGUAGE, KEY_TYPE, KEY_VALUE } from "./constants";
import IRIs from "./IRIs";

const getExportFilename = (schemaGraph, dataGraph) => {
  const exportFileNamePropertyShape = getSelectedShape(
    schemaGraph,
    dataGraph
  )?.[IRIs.roger.exportFileNameProperty]?.[KEY_ID];

  const exportFileNameDataPath = exportFileNamePropertyShape
    ? getShape(schemaGraph, exportFileNamePropertyShape)[IRIs.sh.path][KEY_ID]
    : undefined;

  const filename = dataGraph[exportFileNameDataPath];

  /** this is a fallback if for any reason an array is returned from the datapath instead of a value */
  if (Array.isArray(filename)) return filename[0];
  else return filename;
};

const getNodeShapes = (schema) =>
  schema?.filter((q) => q[KEY_TYPE] === IRIs.sh.NodeShape);

/**
 * Get an array of all PropertyShapes in the Schema.
 * @param {Object[]} schemaGraph
 * @returns
 */
const getPropertyShapes = (schemaGraph) =>
  schemaGraph?.filter((q) => q[KEY_TYPE] === IRIs.sh.PropertyShape);

/**
 * Get an array of all possible target paths defined in the schema.
 *
 * @param {*} schemaGraph
 * @returns {String[]} An array of strings of target class(?) path uris.
 */
const getPropertyShapesPaths = (schemaGraph) =>
  getPropertyShapes(schemaGraph)?.map((shape) => shape[IRIs.sh.path][KEY_ID]);

/**
 * Get the currently selected shape object.
 * @param {*} schemaGraph
 * @param {*} dataGraph
 * @returns {Object} An object representing the selected NodeShape.
 */
const getSelectedShape = (schemaGraph, dataGraph) =>
  getNodeShapes(schemaGraph).find(
    (nodeShape) => nodeShape[KEY_ID] === dataGraph[KEY_TYPE]
  );

/**
 * Get the js shape object from the schemaGraph by its name.
 * @param {Object[]} schemaGraph
 * @param {String} shapeName
 * @returns {Object} The shape object corresponding to the shapeName.
 */
const getShape = (schemaGraph, shapeName) =>
  schemaGraph?.find((q) => q[KEY_ID] === shapeName);

const getShapeProperties = (schema, shape) =>
  shape
    ? getShape(schema, shape)?.[IRIs.sh.property].map((p) => p[KEY_ID])
    : null;

/**
 * Check if data is a reference to another resource, else try to extract data.
 * @param {Object} data
 * @returns
 */
const getData = (data) => getDataId(data) || getDataValue(data) || "";

/**
 * Get the value of the id key of a data object.
 * @param {Object} data
 * @returns {string|undefined} The data id as a string.
 */
const getDataId = (data) => data?.[KEY_ID] || undefined;

/**
 * Get the data type IRI of a propertyShape.
 * May be a node reference (object containing only the @id property), a value of the @type key, or the default.
 * @param {Object} propertyShape
 * @returns {string} The data type as a string representation of the XMLSchema IRI.
 */
const getDataType = (propertyShape) =>
  propertyShape?.[IRIs.sh.nodeKind]
    ? propertyShape[IRIs.sh.nodeKind][KEY_ID]
    : propertyShape[IRIs.sh.datatype][KEY_ID];

/**
 * Get the value of the value key of a data object.
 * @param {Object} data
 * @returns {string}
 */
const getDataValue = (data) => data?.[KEY_VALUE] || data;

/**
 * Get the value of the language key of a data object.
 * @param {Object} data
 * @returns {(string|undefined)}
 */
const getDataLanguage = (data) => data?.[KEY_LANGUAGE] || undefined;

/**
 * Get the first object key by a certain value.
 *
 * @param {Object} obj
 * @param {*} val
 * @returns {string}
 */
const getKeyByValue = (obj, val) => {
  return Object.keys(obj).find((key) => obj[key] === val);
};

export {
  getData,
  getDataLanguage,
  getDataType,
  getDataValue,
  getExportFilename,
  getKeyByValue,
  getNodeShapes,
  getPropertyShapes,
  getPropertyShapesPaths,
  getSelectedShape,
  getShape,
  getShapeProperties,
};
