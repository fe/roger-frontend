// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: CC0-1.0

const KEY_CONTEXT = "@context";
const KEY_ID = "@id";
const KEY_LANGUAGE = "@language";
const KEY_TYPE = "@type";
const KEY_VALUE = "@value";

const RESULTS_PER_PAGE = [5, 10, 50, 100];

const GND_BASE_URL = "/gnd/";
const LOBID_GND_BASE_URL = "/lobid/";
const LOBID_GND_SEARCH_URL = `${LOBID_GND_BASE_URL}search?q=`;
const LOBID_RESOURCES_BASE_URL = "/resources/";
const LOBID_RESOURCES_SEARCH_URL = `${LOBID_RESOURCES_BASE_URL}search?q=`;

export {
  GND_BASE_URL,
  KEY_CONTEXT,
  KEY_ID,
  KEY_LANGUAGE,
  KEY_TYPE,
  KEY_VALUE,
  LOBID_GND_BASE_URL,
  LOBID_GND_SEARCH_URL,
  LOBID_RESOURCES_BASE_URL,
  LOBID_RESOURCES_SEARCH_URL,
  RESULTS_PER_PAGE,
};
