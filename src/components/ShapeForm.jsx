// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import React from "react";
import {
  getData,
  getDataType,
  getShape,
  getShapeProperties,
} from "../utils/getters";
import { KEY_ID } from "../utils/constants";
import { Stack } from "@mui/material";
import EditorFrame from "./editors/EditorFrame";
import IRIs from "../utils/IRIs";
import FieldFrame from "./editors/FieldFrame";
import componentMapping from "./mapping";

export default function ShapeForm({
  data,
  onChange: [handleAdd, handleRemove, handleChange],
  schema,
  shape: nodeShape,
}) {
  /**
   * Get the shape objects for each PropertyShape defined as a property of the
   * selected NodeShape and aggregate them into an array.
   * ATTENTION: This is not a duplicate implementation of utils/getters/getPropertyShapes.
   */
  const propertyShapes = getShapeProperties(schema, nodeShape)?.map(
    (property) => getShape(schema, property)
  );

  return (
    <Stack spacing={3}>
      {propertyShapes?.map((propertyShape) => (
        <EditorFrame
          key={JSON.stringify(propertyShape[IRIs.sh.path][KEY_ID])}
          onChange={handleAdd}
          path={propertyShape[IRIs.sh.path][KEY_ID]}
        >
          {(Array.isArray(data?.[propertyShape[IRIs.sh.path][KEY_ID]])
            ? data[propertyShape[IRIs.sh.path][KEY_ID]]
            : [data[propertyShape[IRIs.sh.path][KEY_ID]]]
          ).map((c, index) => (
            <FieldFrame
              index={index}
              key={index}
              path={propertyShape[IRIs.sh.path][KEY_ID]}
              onChange={handleRemove}
            >
              {React.createElement(
                componentMapping[
                  propertyShape["http://datashapes.org/dash#editor"][KEY_ID]
                ],
                {
                  data: getData(c),
                  dataType: getDataType(propertyShape),
                  onChange: handleChange,
                  index: index,
                  label: propertyShape[IRIs.sh.name],
                  helperText: propertyShape[IRIs.sh.description],
                  path: propertyShape[IRIs.sh.path][KEY_ID],
                  shape: propertyShape, // temp solution; provide filterTerm instead!?
                }
              )}
            </FieldFrame>
          ))}
        </EditorFrame>
      ))}
    </Stack>
  );
}
