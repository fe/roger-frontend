// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { IconButton, InputAdornment, TextField } from "@mui/material";
import SyncIcon from "@mui/icons-material/Sync";
import React from "react";

export default function PrefillInput({
  disabled,
  onSelectRemoteInstance: handleSelectRemoteInstance,
}) {
  /** The ID as typed in by the user. */
  const [userInput, setUserInput] = React.useState("");

  // **************
  // event handlers
  // **************

  /** Set user input. */
  const handleChange = (event) => {
    setUserInput(event.target.value);
  };

  /** Hand up ID on user pressing Enter. */
  const handleKeyPress = async (event) => {
    if (event.key === "Enter") {
      handleSelectRemoteInstance(userInput);
    }
  };

  /** Hand up ID on user clicking SyncIcon. */
  const handleClickSync = () => {
    handleSelectRemoteInstance(userInput);
  };

  return (
    <TextField
      disabled={disabled}
      label={"IRI"}
      helperText={undefined}
      value={userInput}
      onChange={handleChange}
      onKeyPress={handleKeyPress}
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            <IconButton
              disabled={disabled}
              edge="end"
              onClick={handleClickSync}
              title="Sync"
            >
              <SyncIcon />
            </IconButton>
          </InputAdornment>
        ),
      }}
    />
  );
}
