// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import React from "react";
import { Button } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import RemoteInstancesSelectDialog from "./editors/RemoteInstancesSelectDialog";

export default function PrefillButton({
  disabled,
  onSelect: handleSelect,
  shape,
}) {
  /** Visibility of the prefill search dialog. */
  const [open, setOpen] = React.useState(false);

  // **************
  // event handlers
  // **************

  /** Open dialog. */
  const handleOpenDialog = () => {
    setOpen(true);
  };

  return (
    <>
      <Button
        disabled={disabled}
        onClick={handleOpenDialog}
        variant="outlined"
        startIcon={<SearchIcon />}
      >
        Prefill
      </Button>
      <RemoteInstancesSelectDialog
        handleSelect={handleSelect}
        isOpen={[open, setOpen]}
        shape={shape}
      />
    </>
  );
}
