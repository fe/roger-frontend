// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import React, { useEffect } from "react";
import { Button, ButtonGroup, Container, Stack } from "@mui/material";

import { v4 as uuidv4 } from "uuid";

import { endPromise, parsePromise } from "../promises/N3Promises";
import SelectShape from "./SelectShape";
import {
  getExportFilename,
  getNodeShapes,
  getPropertyShapesPaths,
  getSelectedShape,
} from "../utils/getters";
import ShapeForm from "./ShapeForm";
import { getItem } from "../utils/queryAuthorities";
import { KEY_ID, KEY_TYPE, KEY_VALUE } from "../utils/constants";
import IRIs from "../utils/IRIs";
import PrefillInput from "./PrefillInput";

const N3 = require("n3");
const parseTurtleToJSONld = require("@frogcat/ttl2jsonld").parse;

/**
 * This is the top level of the ROGER component.
 *
 * Describe its API here and move subcomponents and implementation details to other files.
 * STOP and think about it before changing the API.
 */
export default function Roger({ data, onSave: handleSave, schema }) {
  /**
   * Parse data into a N3.store, write a normalized turtle from its quads,
   * and parse it to jsonld.
   * @param {string} input
   * @returns
   */
  const parse = async (input) => {
    const store = new N3.Store();
    const parser = new N3.Parser();
    const writer = new N3.Writer();

    await parsePromise(parser, input, store);
    writer.addQuads(store.getQuads());
    const result = await endPromise(writer);

    return parseTurtleToJSONld(result);
  };

  // ******
  // states
  // ******

  /** JSON-LD representation of the data. */
  const [dataGraph, setDataGraph] = React.useState();
  /** JSON-LD representation of the schema. */
  const [schemaGraph, setSchemaGraph] = React.useState();
  /** The currently selected NodeShape. */
  const [selectedShape, setSelectedShape] = React.useState("");

  // *************************************
  // effects depending on props
  // *************************************

  /**
   * Normalize, parse, and store data in the dataGraph state.
   */
  useEffect(() => {
    (async () => {
      let result;
      try {
        result = await parse(data);
      } catch (error) {
        console.log(error.message); // TODO: handle messaging...
      }
      if (result) {
        setDataGraph(result);
      } else {
        setDataGraph({ [KEY_ID]: `roger:${uuidv4()}` });
      }
    })();
  }, [data]);

  /**
   * Normalize, parse, and store schema in the schemaGraph state.
   */
  useEffect(() => {
    (async () => {
      let result;
      try {
        result = await parse(schema);
      } catch (error) {
        console.log(error.message); // TODO: handle messaging...
      }
      if (result) setSchemaGraph(result["@graph"]);
    })();
  }, [schema]);

  // **************
  // event handlers
  // **************

  /** Clear the dataGraph. */
  const handleClickClear = () => {
    // keep the ID:
    // setDataGraph({ [KEY_ID]: dataGraph[KEY_ID], [KEY_TYPE]: selectedShape });
    // regenerate ID on clear:
    setDataGraph({ [KEY_ID]: `roger:${uuidv4()}`, [KEY_TYPE]: selectedShape });
  };

  const handleSelectShape = (shape) => {
    /**
     * Generate a dataGraph stub with a random ID if no data (with ID) is given
     * or parsing of the data failed for some reason. Add/update the data type
     * of the selected NodeShape.
     */
    // TODO: resolve duplicate state of shape
    setDataGraph({
      ...dataGraph,
      [KEY_TYPE]: shape,
    });
    setSelectedShape(shape);
  };

  /** Write the dataGraph into a blob and induce a file download in the browser. */
  const handleClickExport = () => {
    const data = JSON.stringify(dataGraph, null, 2);
    const blob = new Blob([data]);
    const downloadUrl = URL.createObjectURL(blob);
    let link = document.createElement("a");
    const filename = getExportFilename(schemaGraph, dataGraph);
    if (filename) link.download = `${filename}.json`;
    else link.download = "export.json";
    link.href = downloadUrl;
    link.click();
    window.URL.revokeObjectURL(downloadUrl);
  };

  /** Handle input data change. */
  const handleChange = (path, value, type, index) => {
    /** Format the value depending on its type.*/
    let formattedValue;
    switch (type) {
      // string type is jsonld default and omits "type"
      case IRIs.xsd.string:
        formattedValue = value;
        break;
      // IRI/URI types
      case IRIs.sh.IRI:
      case IRIs.xsd.anyURI:
        formattedValue = { [KEY_ID]: value };
        break;
      // generic format for all other cases
      default:
        formattedValue = { [KEY_VALUE]: value, [KEY_TYPE]: type };
    }

    setDataGraph({
      ...dataGraph,
      [path]: [
        ...(Array.isArray(dataGraph[path])
          ? dataGraph[path]
          : [dataGraph[path]]
        ).slice(0, index),
        formattedValue,
        ...(Array.isArray(dataGraph[path])
          ? dataGraph[path]
          : [dataGraph[path]]
        ).slice(index + 1),
        ...[],
      ],
    });
  };

  /**
   * Prepare data to be handed up to the parent component and, ultimately, call the save-handler handed down in the onSave prop.
   * This currently is a stub.
   */
  const handleClickSave = () => {
    /* ... */
    handleSave(dataGraph);
  };

  const handleAdd = (path) => {
    setDataGraph({
      ...dataGraph,
      [path]: [
        ...(Array.isArray(dataGraph[path])
          ? dataGraph[path]
          : [dataGraph[path]]),
        null,
        ...[],
      ],
    });
  };

  const handleRemove = (path, index) => {
    // may result in empty array instead of null
    setDataGraph({
      ...dataGraph,
      [path]: Array.isArray(dataGraph[path])
        ? [
            ...dataGraph[path].slice(0, index),
            ...dataGraph[path].slice(index + 1),
          ]
        : null,
    });
  };

  /**
   * Fetch data, parse it, and insert it into the dataGraph.
   * @param {*} iri
   */
  const handleSelectRemoteInstance = async (iri) => {
    const id = iri.split("/").pop();
    const shape = getSelectedShape(schemaGraph, dataGraph);
    const urlTemplate = shape[IRIs.roger.prefillUrlTemplate];
    let response;
    try {
      response = await getItem(urlTemplate, id);
    } catch (error) {
      console.log(error.message); // TODO: handle messaging...
      return;
    }

    const data = response.data;
    const parser = new N3.Parser();
    const store = new N3.Store();
    try {
      await parsePromise(parser, data, store);
    } catch (error) {
      console.log(error.message); // TODO: handle messaging...
      return;
    }

    const paths = getPropertyShapesPaths(schemaGraph);
    const filteredStore = new N3.Store();
    paths?.forEach((path) =>
      filteredStore.addQuads(
        store.getQuads(iri).filter((q) => q.predicate.id === path)
      )
    );

    const writer = new N3.Writer();
    writer.addQuads(filteredStore.getQuads());
    let result;
    try {
      result = await endPromise(writer);
    } catch (error) {
      console.log(error.message); // TODO: handle messaging...
      return;
    }

    let parsed;
    try {
      parsed = parseTurtleToJSONld(result);
    } catch (error) {
      console.log(error.message); // TODO: handle messaging...
      return;
    }

    if (parsed)
      setDataGraph((dataGraph) => ({
        ...parsed,
        [KEY_ID]: dataGraph[KEY_ID],
        [KEY_TYPE]: dataGraph[KEY_TYPE],
      }));
  };

  return (
    <Container>
      {schemaGraph ? (
        <Stack spacing={2}>
          <Stack direction="row" spacing={2}>
            <SelectShape
              shapes={getNodeShapes(schemaGraph)}
              selectedShape={selectedShape}
              onSelectShape={handleSelectShape}
            />
            <PrefillInput
              disabled={
                !getSelectedShape(schemaGraph, dataGraph)?.[
                  IRIs.roger.prefillUrlTemplate
                ]
              }
              onSelectRemoteInstance={handleSelectRemoteInstance}
            />
          </Stack>
          {selectedShape ? (
            <>
              <ShapeForm
                schema={schemaGraph}
                shape={selectedShape}
                data={dataGraph}
                onChange={[handleAdd, handleRemove, handleChange]}
              ></ShapeForm>
              <ButtonGroup>
                <Button onClick={handleClickClear}>Clear</Button>
                <Button onClick={handleClickExport}>Export</Button>
                <Button disabled={!handleSave} onClick={handleClickSave}>
                  Save
                </Button>
              </ButtonGroup>
            </>
          ) : (
            "Select a Shape."
          )}
        </Stack>
      ) : (
        "Schema empty or corrupt."
      )}
    </Container>
  );
}
