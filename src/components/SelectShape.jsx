// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import React from "react";
import { KEY_ID } from "../utils/constants";
import IRIs from "../utils/IRIs";

export default function SelectShape({ shapes, selectedShape, onSelectShape: handleSelectShape }) {
  const SELECT_LABEL = "Shape";
  const SELECT_LABEL_ID = "select-shape-label";

  return (
    <FormControl fullWidth>
      {/* MUI-specific: To properly label your Select input you need an extra element with an id that contains a label. That id needs to match the labelId of the Select e.g. */}
      <InputLabel id={SELECT_LABEL_ID}>{SELECT_LABEL}</InputLabel>
      <Select
        label={SELECT_LABEL}
        labelId={SELECT_LABEL_ID}
        value={selectedShape}
        onChange={(event) => handleSelectShape(event.target.value)}
      >
        {/* TODO: filter for roger:formNode false ; */}
        {shapes.map((q) => (
          <MenuItem key={JSON.stringify(q)} value={q[KEY_ID]}>
            {q[IRIs.sh.name] ||
              q[IRIs.rdfs.label]}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
