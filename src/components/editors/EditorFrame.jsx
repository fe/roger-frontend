// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { IconButton, Stack } from "@mui/material";
import React from "react";
import AddBoxIcon from "@mui/icons-material/AddBox";

/**
 *  * A component framing multiple field frames and the add button.
 *
 */
export default function EditorFrame({
  children,
  path,
  onChange: handleAdd,
}) {
  return (
    <Stack spacing={2} sx={{ p: 2, border: "1px dashed grey" }}>
      {children}
      <IconButton onClick={() => handleAdd(path)}>
        <AddBoxIcon />
      </IconButton>
    </Stack>
  );
}
