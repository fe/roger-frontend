// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import React from "react";

import { TextField } from "@mui/material";

export default function TextAreaEditor({
  data,
  dataType,
  onChange: handleChange,
  helperText,
  index,
  label,
  path,
}) {
  return (
    <TextField
      label={label}
      helperText={helperText}
      value={data}
      onChange={(event) => handleChange(path, event.target.value, dataType, index)}
      fullWidth
      multiline
    />
  );
}
