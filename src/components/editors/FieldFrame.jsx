// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { IconButton, Stack } from "@mui/material";
import React from "react";
import DeleteIcon from "@mui/icons-material/Delete";

/**
 * A component framing one input field of different kinds and the delete button.
 *
 */
export default function FieldFrame({
  children,
  index,
  path,
  onChange: handleRemove,
}) {
  return (
    <Stack direction="row">
      {children}
      <IconButton onClick={() => handleRemove(path, index)}>
        <DeleteIcon />
      </IconButton>
    </Stack>
  );
}
