// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: CC0-1.0

// Button is used as a fallback for components not implemented yet
import { Button } from "@mui/material";
import AutoCompleteEditor from "./editors/AutoCompleteEditor";
import RemoteInstancesSelectEditor from "./editors/RemoteInstancesSelectEditor";
import TextAreaEditor from "./editors/TextAreaEditor";
import TextFieldEditor from "./editors/TextFieldEditor";
import URIEditor from "./editors/URIEditor";

const componentMapping = {
  "http://datashapes.org/dash#AutoCompleteEditor": AutoCompleteEditor,
  "http://datashapes.org/dash#BlankNodeEditor": Button,
  "http://datashapes.org/dash#BooleanSelectEditor": Button,
  "http://datashapes.org/dash#DatePickerEditor": Button,
  "http://datashapes.org/dash#DateTimePickerEditor": Button,
  "http://datashapes.org/dash#DetailsEditor": Button,
  "http://datashapes.org/dash#EnumSelectEditor": Button,
  "http://datashapes.org/dash#InstancesSelectEditor": Button,
  "http://datashapes.org/dash#RichTextEditor": Button,
  "http://datashapes.org/dash#SubClassEditor": Button,
  "http://datashapes.org/dash#TextAreaEditor": TextAreaEditor,
  "http://datashapes.org/dash#TextAreaWithLangEditor": Button,
  "http://datashapes.org/dash#TextFieldEditor": TextFieldEditor,
  "http://datashapes.org/dash#TextFieldWithLangEditor": Button,
  "http://datashapes.org/dash#URIEditor": URIEditor,
  "https://www.sub.uni-goettingen.de/roger/schema#SelectRemoteInstanceEditor": RemoteInstancesSelectEditor,
};

export default componentMapping;
