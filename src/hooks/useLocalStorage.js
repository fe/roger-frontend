// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { useState } from "react";

/**
 * Check if the localStorage is available.
 * Adopted and adapted from [MDN Docs](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API#testing_for_availability).
 * @returns {boolean} True if localStorage is available.
 */
function localStorageAvailable() {
  let storage;
  try {
    storage = window["localStorage"];
    const x = "__storage_test__";
    storage.setItem(x, x);
    storage.removeItem(x);
    return true;
  } catch (e) {
    return (
      e instanceof DOMException &&
      // everything except Firefox
      (e.code === 22 ||
        // Firefox
        e.code === 1014 ||
        // test name field too, because code might not be present
        // everything except Firefox
        e.name === "QuotaExceededError" ||
        // Firefox
        e.name === "NS_ERROR_DOM_QUOTA_REACHED") &&
      // acknowledge QuotaExceededError only if there's something already stored
      storage &&
      storage.length !== 0
    );
  }
}

/**
 * Make use of the browser's local Storage.
 * Adopted and adapted from [useHooks](https://usehooks.com/useLocalStorage/).
 * @param {string} key A name for the object to be stored in.
 * @param {*} initialValue A value with which the storage object is initially created.
 * @returns {[Object, Function]} Returns value stored in local Storage, and a function to update it.
 */
export default function useLocalStorage(key, initialValue) {
  // State to store our value
  // Pass initial state function to useState so logic is only executed once.
  const [storedValue, setStoredValue] = useState(() => {
    if (localStorageAvailable()) {
      try {
        const item = window.localStorage.getItem(key);
        return item ? JSON.parse(item) : initialValue;
      } catch (error) {
        console.log(error);
        return initialValue;
      }
    } else {
      return initialValue;
    }
  });

  /**
   * A wrapped version of useState's setter function that persists the new value to localStorage.
   * @param {*} value
   */
  const setValue = (value) => {
    try {
      // Allow value to be a function for an API like with useState.
      const valueToStore =
        value instanceof Function ? value(storedValue) : value;
      setStoredValue(valueToStore);
      if (localStorageAvailable()) {
        window.localStorage.setItem(key, JSON.stringify(valueToStore));
      }
    } catch (error) {
      console.log(error);
    }
  };

  return [storedValue, setValue];
}
