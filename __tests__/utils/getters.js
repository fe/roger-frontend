// SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
//
// SPDX-License-Identifier: EUPL-1.2

import { getDataType } from "../../src/utils/getters";

import { KEY_TYPE } from "../../src/utils/constants";

// value and type are not replaced with constants in test data so that the
// tests fail when the constants are wrong/have a typo
const dataWithType = JSON.parse(
  '{ "@value": "806", "@type": "http://www.w3.org/2001/XMLSchema#date" }'
);
test("get data type from sampledata with type", () => {
  expect(getDataType(dataWithType)).toBe(dataWithType[KEY_TYPE]);
});

const dataWithoutType = JSON.parse('"Abū-Tammām Ḥabīb Ibn-Aus aṭ-Ṭāʾī"');
test("get data type from sampledata without type", () => {
  expect(getDataType(dataWithoutType)).toBe("string");
});
